@extends('layouts.fullwidth')

{{-- Web site Title --}}
@section('title')
Calendar View
@stop

@section('styles')
<link rel='stylesheet' type='text/css' href="{{ asset('js/ext/fullcalendar/fullcalendar.css') }}" />
<link rel='stylesheet' type='text/css' href="{{ asset('css/calendar.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/schedule.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/map-suggest-inline.css') }}" >
<link rel='stylesheet' type='text/css' href="{{ asset('css/bootstrap-datepicker3.css') }}" >

@stop

{{-- Content --}}
@section('content')
@foreach($profiles as $profile) 
	<input type="hidden" id="start-location-uid-{{ $profile['id'] }}" value="{{ $profile['start_location'] }}">
	<input type="hidden" id="color-uid-{{ $profile['id'] }}" value="{{ $profile['color'] }}">
@endforeach

<div class="well well-sm add-cust-container calendar-top-bar" id="mainContainer">
	<div class="avatarList">
		@foreach ($profiles as $seller)
			<img src="{{ '/img/'.$seller['avatar'] }}" />
			<input type="checkbox" id="visiblebox{{ $seller['id'] }}" value="{{ $seller['id'] }}" name="salesTeam[]" @if ($seller['visible'] == '1') checked @endif > {{ $seller['first_name'] }}
		@endforeach
	</div>
	<form id="newLeadForm" class="form-inline" action="/customers/newLead" method="POST">
		<div data-address-group="jobsite" data-open-in-gmap>
			Add Cust
			<input placeholder="Last Name" id="l_name" class="input-small typeahead form-control" name="l_name" value="" autocomplete="off" required="" type="text">
			<input placeholder="First Name" id="f_name" class="input-small form-control" name="f_name" value="" required="" type="text">
			<input placeholder="Company" id="company_name" class="input-small form-control" name="company_name" value="" required="" type="text">
			<?php echo Form::text('phone', '', array('placeholder' => 'Phone', 'class' => 'input-small form-control', 'id' => 'phone')); ?>
			<input placeholder="Jobsite Address" id="job_address" class="input-small address form-control map-suggest-address" name="address" value="" required="" type="text" style="width: 243px">
			<?php echo Form::text('city', '', array('placeholder' => 'City', 'id' => 'job_city', 'class' => 'input-small form-control city map-suggest-city')); ?>
			<input placeholder="Zip" class="input-small zip form-control map-suggest-zip" name="zip" id="zip" value="" type="text">
			<?php echo Form::text('built', '', array('placeholder' => 'Yr Built', 'class' => 'input-small form-control built', 'id' => 'built')); ?>
			{{ Form::button('<i id="expandGlyph" class="glyphicon glyphicon-chevron-down"></i>', ['class' => 'btn btn-small btn-success', 'id' => 'btnMore']) }}
			{{ Form::button('Map <i id="expandGlyph" class="glyphicon glyphicon-plus"></i>', ['class' => 'btn btn-small btn-primary', 'id' => 'btnMap']) }}
		</div>
		<div class="row" id="collapseBox">
			<div id="divMore" class="collapse">
				<?php echo Form::text('alt_phone', '', array('placeholder' => 'Alt. Phone', 'class' => 'input-small form-control', 'id' => 'alt_phone')); ?>
				<?php echo Form::text('email', '', array('placeholder' => 'EMail', 'class' => 'input-small form-control', 'id' => 'email')); ?>
						<input id="sourceCheckbox1" value="Angies" name="lead_source[]" type="checkbox"> Angies&nbsp;&nbsp;
						<input id="sourceCheckbox2" value="Yelp" name="lead_source[]" type="checkbox"> Yelp&nbsp;&nbsp;
						<input id="sourceCheckbox3" value="Google" name="lead_source[]" type="checkbox"> Google&nbsp;&nbsp;
						<input id="sourceCheckbox4" value="LA Conservancy" name="lead_source[]" type="checkbox"> LA Conserv.&nbsp;&nbsp;
						<input id="sourceCheckbox5" value="Other" name="lead_source[]" type="checkbox"> Referral or Other:
						<?php echo Form::text('source_referral', '', array('placeholder' => 'Note Referral or Other', 'class' => 'form-control', 'name' => 'source_referral', 'id' => 'source_referral', 'disabled' => 'disabled')); ?>        
				<div>
					<textarea placeholder="Notes" id="note" value="" class="textarea form-control" rows="1" name="note"></textarea>
					{{ Form::button('Billing Detail', ['class' => 'btn btn-small btn-success', 'id' => 'btnBillDetail']) }}
				</div>
			</div>
			<div id="divBillDetail" class="collapse" data-address-group="billing">
				{{ Form::text('billing_address', '', array('placeholder' => 'Billing Address', 'class' => 'form-control input-small map-suggest-address', 'id' => 'billing_add')) }}
				{{ Form::text('billing_city', '', array('placeholder' => 'Billing City', 'class' => 'input-small city form-control map-suggest-city', 'id' => 'billing_city')) }}
				{{ Form::text('billing_state', '', array('placeholder' => 'State', 'class' => 'input-small state form-control map-suggest-state', 'id' => 'billing_state')) }}
				{{ Form::text('billing_zip', '', array('placeholder' => 'Zip', 'class' => 'input-small form-control map-suggest-zip', 'id' => 'billing_zip')) }}
			</div>
		</div>
		<div id="jobList" class="accordion-body collapse">
			<h4><em>Prior Jobs</em></h4> 
			<table class="table table-condensed table-striped">
				<thead>
					<tr><th>&nbsp;&nbsp;</th>
					<th>Job ID</th>
					<th>Address</th>
					<th>City</th>
					<th>Est. Scheduled</th>
					<th>Job Scheduled</th>
					<th>Job Completed</th>
				</tr></thead>
				<tbody id="jobs_table">
				</tbody>
			</table>
			<button class="btn btn-primary btn-sm" type="button" onclick="copyButton()" id="copyButton">Copy info to new job</button>
			{{ Form::button('Cancel', ['class' => 'btn btn-sm btn-danger', 'id' => 'btnCopyCancel']) }}
		</div>
	</form>
</div>

<div class="container-fluid">
	<div id="calendar">
	</div>
	<div id="map_day_container" class="container-fluid mapDayContainer">
		<span class="map_day" id="map_day"></span>
	</div>
</div>
<div class="container" id="directions"></div>

<div class="modal fade" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">
		<form id="scheduleformID" class="form-inline" action="/customers/calNewLead" method="post">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				<h4 class="modal-title">Lead Information</h4>
			</div>
			<div class="modal-body">
				<div class="sysdesc">&nbsp;</div>
				<div class="row">
					<div class="col-md-2"><label class="scheduleLabel control-label">Starting:</label></div>
					<div class="col-md-10">
						<span id="startTimeSpan"></span>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2"><label class="scheduleLabel control-label">Ending:</label></div>
					<div class="col-md-10">
						<span id="endTimeSpan"></span>
					</div>
				</div>
				<div class="row">
					<div class="span2 calendar">
						<div class="col-md-2">
							{{ Form::label('calendarID', 'Calendar:', array('class' => 'scheduleLabel control-label')) }}
						</div>
						<div class="col-md-10">
							{{ Form::select('calendarID', $calendar, '', array('class' => 'form-control')) }}
							{{ Form::hidden('start', '', array('id' => 'start')) }}
							{{ Form::hidden('end', '', array('id' => 'end')) }}
						</div>
					</div>
					<div class="span3 event_type">
						<div class="col-md-2">
							{{ Form::label('event_type', 'Event Type:', array('class' => 'scheduleLabel control-label')) }}
						</div>
						<div class="col-md-10">
							{{ Form::select('event_type', [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call', EVENT_PERSONAL_TIME => 'Personal or Office'], '', array('class' => 'form-control')) }}
						</div>
					</div>
					<div class="span3 customer_type">
						<div class="col-md-2">
							{{ Form::label('customer_type', 'Customer:', array('class' => 'scheduleLabel control-label')) }}
						</div>
						<div class="col-md-10">
							<label class="existing"><input type="radio" name="customer_type" value="existing"> Existing</label>
							<label class="new"><input type="radio" name="customer_type" value="new"> New</label>
						</div>
					</div>
				</div>
				<div class="row existing-customer-row">
					<div class="mt5">
						<div class="col-md-2">
							{{ Form::label('choose_cust', 'Choose Job:', array('class' => 'scheduleLabel control-label')) }}
						</div>
						<div class="col-md-10">
							<select class="form-control" name="job_id">
								<option value="0">Choose...</option>
								@foreach($init_customers as $customer)
									<option value="{{ $customer->job_id }}">Job# {{ $customer->job_id }} - {{ implode(', ', [$customer->l_name, $customer->f_name]) }}{{ !empty($customer->company_name) ? ' (' . $customer->company_name . ')' : '' }} - {{ implode(', ', [$customer->address, $customer->city]) }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
				<div class="row new-customer-row">
					<div class="mt5">
						<div class="col-md-2">
							{{ Form::label('add_cust', 'Add Cust:', array('class' => 'scheduleLabel control-label')) }}
						</div>
						<div class="col-md-10">
							<input placeholder="Last Name *" id="l_name2" class="input-small form-control" name="l_name" value="" autocomplete="off" type="text" style="width: 150px" required>
							<input placeholder="First Name *" id="f_name2" class="input-small form-control" name="f_name" value="" type="text" style="width: 150px" required>
							<input placeholder="Company Name" id="company_name2" class="input-small form-control" name="company_name" value="" type="text" style="width: 150px">
							<?php echo Form::text('phone', '', array('placeholder' => 'Phone', 'class' => 'input-small form-control', 'id' => 'phone2')); ?>

							<div class="mt5" data-address-group="lead_info_address">
								<input placeholder="Jobsite Address *" id="job_address2" class="input-small address form-control map-suggest-address" name="address" value="" type="text" required>
								<?php echo Form::text('city', '', array('placeholder' => 'City', 'id' => 'job_city2', 'class' => 'input-small form-control city map-suggest-city')); ?>
								<input placeholder="Zip" class="input-small zip form-control map-suggest-zip" name="zip" id="zip2" value="" type="text">
							</div>
							<div id="divMore2" class=" mt5">
								<?php echo Form::text('email', '', array('placeholder' => 'EMail', 'class' => 'input-small form-control', 'id' => 'email2', 'style' => 'width: 250px;')); ?>
								<?php echo Form::text('alt_phone', '', array('placeholder' => 'Alt. Phone', 'class' => 'input-small form-control', 'id' => 'alt_phone2')); ?>
								<?php echo Form::text('built', '', array('placeholder' => 'Yr Built', 'class' => 'input-small form-control built', 'id' => 'built2')); ?>
							</div>
							<div class="mt5">
								<input id="sourceCheckbox1" value="Angies" name="lead_source[]" type="checkbox"> Angies&nbsp;&nbsp;
								<input id="sourceCheckbox2" value="Yelp" name="lead_source[]" type="checkbox"> Yelp&nbsp;&nbsp;
								<input id="sourceCheckbox3" value="Google" name="lead_source[]" type="checkbox"> Google&nbsp;&nbsp;
								<input id="sourceCheckbox4" value="LA Conservancy" name="lead_source[]" type="checkbox"> LA Conserv.&nbsp;&nbsp;
							</div>
							<div class="mt5">
								<input id="sourceCheckbox5a" value="Other" name="lead_source[]" type="checkbox"> Referral or Other:
								<?php echo Form::text('source_referral', '', array('placeholder' => 'Note Referral or Other', 'class' => 'form-control', 'name' => 'source_referral', 'id' => 'source_referral2', 'disabled' => 'disabled')); ?>
								{{ Form::button('Billing Detail', ['class' => 'btn btn-small btn-success', 'id' => 'btnBillDetail2']) }}
							</div>
							<div id="divBillDetail2" class="collapse mt5">
								{{ Form::text('billing_address', '', array('placeholder' => 'Billing Address', 'class' => 'form-control input-small', 'id' => 'billing_add2')) }}
								{{ Form::text('billing_city', '', array('placeholder' => 'Billing City', 'class' => 'input-small city form-control', 'id' => 'billing_city2')) }}
								{{ Form::text('billing_state', '', array('placeholder' => 'State', 'class' => 'input-small state form-control', 'id' => 'billing_state2')) }}
								{{ Form::text('billing_zip', '', array('placeholder' => 'Zip', 'class' => 'input-small form-control', 'id' => 'billing_zip2')) }}
							</div>

						</div>
					</div>
					
				</div>
				<textarea placeholder="Notes" id="note2" value="" class="textarea form-control lead-info-note" rows="3" name="note"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<a class="btn btn-success" id="btnEditJobCal" target="_blank">Edit Job</a>
				{{ Form::submit('Schedule Event', array('class' => 'btn btn-primary', 'id' => 'btnSchedEventCal','disabled' => 'disabled')) }}
			</div>
		</FORM>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="jobsModal">
  <div class="modal-dialog">
    <div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h4 class="modal-title">Map Event to Job</h4>
		</div>
		<div class="modal-body">
			<div class="sysdesc">&nbsp;</div>
			<div class="row"><p><b>Event</b>: <span id="eventTitle"></span></p></div>
			<div class="row"><p><b>Start</b>: <span id="eventStart"></span></p></div>
			<div class="row"><p><b>End</b>: <span id="eventEnd"></span></p></div>
			<hr style="margin-left: 0; margin-right: 0">
			<div class="row customer_type">
				{{ Form::label('customer_type', 'Job:', array('class' => 'scheduleLabel control-label')) }}
				<label class="existing"><input type="radio" name="customer_type" value="existing"> Existing</label>
				<label class="new"><input type="radio" name="customer_type" value="new"> New</label>
			</div>
			<div class="row new-customer-row form-inline">
				<form class="map-event-new-job-form">
					<hr style="margin-left: 0; margin-right: 0">
					<div class="mt5">
						{{ Form::label('add_cust', 'Add Cust:', array('class' => 'scheduleLabel control-label')) }}
						<input placeholder="Last Name" id="l_name3" class="input-small form-control" name="l_name" value="" autocomplete="off" type="text" style="width: 150px">
						<input placeholder="First Name" id="f_name3" class="input-small form-control" name="f_name" value="" type="text" style="width: 150px">
						<?php echo Form::text('phone', '', array('placeholder' => 'Phone', 'class' => 'input-small form-control', 'id' => 'phone3')); ?>
					</div>
					<div class="mt5" data-address-group="lead_info_address">
						<input placeholder="Jobsite Address" id="job_address3" class="input-small address form-control map-suggest-address" name="address" value="" type="text">
						<?php echo Form::text('city', '', array('placeholder' => 'City', 'id' => 'job_city3', 'class' => 'input-small form-control city map-suggest-city')); ?>
						<input placeholder="Zip" class="input-small zip form-control map-suggest-zip" name="zip" id="zip3" value="" type="text">
					</div>
					<div id="divMore3" class=" mt5">
						<?php echo Form::text('email', '', array('placeholder' => 'EMail', 'class' => 'input-small form-control', 'id' => 'email3', 'style' => 'width: 250px;')); ?>
						<?php echo Form::text('alt_phone', '', array('placeholder' => 'Alt. Phone', 'class' => 'input-small form-control', 'id' => 'alt_phone3')); ?>
						<?php echo Form::text('built', '', array('placeholder' => 'Yr Built', 'class' => 'input-small form-control built', 'id' => 'built3')); ?>
					</div>
					<div class="mt5">
						<textarea placeholder="Notes" id="note3" value="" class="textarea form-control" rows="3" name="note"></textarea>
					</div>
					<div class="mt5">
						<input value="Angies" name="lead_source[]" type="checkbox"> Angies&nbsp;&nbsp;
						<input value="Yelp" name="lead_source[]" type="checkbox"> Yelp&nbsp;&nbsp;
						<input value="Google" name="lead_source[]" type="checkbox"> Google&nbsp;&nbsp;
						<input value="LA Conservancy" name="lead_source[]" type="checkbox"> LA Conserv.&nbsp;&nbsp;
					</div>
					<div class="mt5">
						<input value="Other" name="lead_source[]" type="checkbox"> Referral or Other:
						<?php echo Form::text('source_referral', '', array('placeholder' => 'Note Referral or Other', 'class' => 'form-control', 'name' => 'source_referral', 'id' => 'source_referral3', 'disabled' => 'disabled')); ?>
						{{ Form::button('Billing Detail', ['class' => 'btn btn-small btn-success', 'id' => 'btnBillDetail3']) }}
					</div>
					<div id="divBillDetail3" class="collapse mt5">
						{{ Form::text('billing_address', '', array('placeholder' => 'Billing Address', 'class' => 'form-control input-small', 'id' => 'billing_add3')) }}
						{{ Form::text('billing_city', '', array('placeholder' => 'Billing City', 'class' => 'input-small city form-control', 'id' => 'billing_city3')) }}
						{{ Form::text('billing_state', '', array('placeholder' => 'State', 'class' => 'input-small state form-control', 'id' => 'billing_state3')) }}
						{{ Form::text('billing_zip', '', array('placeholder' => 'Zip', 'class' => 'input-small form-control', 'id' => 'billing_zip3')) }}
					</div>
				</form>
			</div>
			<div class="row existing-customer-row">
				<hr style="margin-left: 0; margin-right: 0">
				<div class="mt5">
					{{ Form::label('choose_cust', 'Choose Job:', array('class' => 'scheduleLabel control-label')) }}
					<select id="map_job_id" class="form-control" name="job_id">
						<option value="0">Choose...</option>
						@foreach($init_customers as $customer)
							<option value="{{ $customer->job_id }}">Job# {{ $customer->job_id }} - {{ implode(', ', [$customer->l_name, $customer->f_name]) }} - {{ implode(', ', [$customer->address, $customer->city]) }}</option>
						@endforeach
					</select>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			<button type="button" class="btn btn-success" id="btnMapEvent">OK</button>
		</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="eventModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
				<h6 id="modalJobId"></h6>
            </div>
            <div class="modal-body">
                <button class="btn btn-info" id="btnJobDetail"><a id="eventJobId" style="color: #ffffff" target="_blank" >Job Detail</a></button>
                <button class="btn btn-info" id="btnMapEventJob"><a id="eventMapEvent" style="color: #ffffff" target="_blank">Map Event to Job</a></button>
				<button class="btn btn-info" id="btnGoogDetail"><a id="eventGoogDetail" style="color: #ffffff" target="_blank">Google Event</a></button>
 <!--               <button class="btn btn-info" id="btnEventEdit" data-toggle="collapse" data-target="#eventData"><a id="eventEdit" style="color: #ffffff" target="_blank">Edit Event</a></button>
-->				<div id="divEventBtn" class="btn-group">
					<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Event <span class="caret"></span>
					</button>
					<ul class="dropdown-menu">
						<li><a data-toggle="collapse" href="#eventData">Edit Event</a></li>
						<li><a data-toggle="collapse" href="#dateSelect">Duplicate Event</a></li>
					</ul>
				</div>
<!--				<button class="btn btn-info" id="btnDupEvent" data-toggle="collapse" data-target="#dateSelect"><a id="dupEvent" style="color: #ffffff" target="_blank">Duplicate Event</a></button>
-->				{{ Form::button('Delete Event', array('id' => 'btnDelete', 'class' => 'btn btn-small btn-danger')) }}
				<button class="btn btn-info" id="btnPartsList"><a id="jobPartsList" style="color: #ffffff" target="_blank">Parts List</a></button>
			</div>
			{{ Form::hidden('eventID', null, array('id' => 'hiddenEventID')) }}
			<div class="collapse" id="eventData">
					{{ Form::open(array('url' => 'foo/bar', 'class' => 'frmEditEvent')) }}
					{{ Form::label('selectCalendar', 'Calendar', array('class' => 'event-label')) }}
					{{ Form::select('google_cal_id', $calendar, null, array('class' => 'formSelect form-control frmEditEvent', 'id' => 'selectCalendar')) }}
					{{ Form::label('selectEvent', 'Event Type', array('class' => 'event-label', 'id' => 'lblType')) }}
					{{ Form::select('type', [EVENT_EST_APPT => 'Estimate Appt.', EVENT_INSTALL_REPAIR => 'Install/Repair Job', EVENT_SERVICE_CALL => 'Service Call'], null, array('class' => 'formSelect form-control frmEditEvent', 'id' => 'selectEvent')) }}
					<div class="buttonMargin">	
						{{ Form::button('Submit', array('class' => 'btn btn-success', 'id' => 'btnEditSubmit')) }}
					</div>
					{{ Form::close() }}
			</div>
			<div class="collapse" id="dateSelect">
				{{ Form::open(array('url' => 'event-duplicate')) }}
				{{ Form::label('date', 'Date', array('class' => 'event-label')) }}
				{{ Form::text('date', null, array('class' => 'form-control', 'id' => 'datePick')) }}
				<div class="buttonMargin">	
					{{ Form::button('Submit', array('class' => 'btn btn-success', 'id' => 'btnDupSubmit')) }}
				</div>
				{{ Form::close() }}
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div id="modalYouSure" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content" id="mdlYouSure">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
				<h4 id="modalTitle" class="modal-title">Are you sure?</h4>
			</div>
			<div id="modalBody" class="modal-body">
				{{ Form::open(array('url' => 'foo/bar', 'id' => 'confirmDelete', 'method' => 'GET')) }}
				{{ Form::button('Cancel', array('class' => 'btn btn-info', 'data-toggle' => 'collapse', 'data-target' => '#modalYouSure', 'data-dismiss' => 'modal')) }}
				{{ Form::button('Confirm Delete', array('class' => 'btn btn-danger', 'id' => 'btnConfirmDelete')) }}
				{{ Form::close() }}
			</div>
		</div>
	</div>
</div>


<div class="expanded-map-outer"><button class="btn btn-primary closeExpandedMapBtn" title="Close">Close</button></div>
@stop

@section('scripts')
<script type='text/javascript' src="{{ asset('js/ext/bootstrap-datepicker.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/fullcalendar/fullcalendar.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/maps.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/schedule.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/typeahead/typeahead.bundle.js') }}"></script> 
<script type='text/javascript' src="{{ asset('js/custTypeAhead.js') }}"></script> 
<script type='text/javascript' src="{{ asset('js/ext/handlebars/handlebars-v1.3.0.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/moment.min.js') }}"></script>
<!--<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places&sensor=false"></script> -->
<script type='text/javascript' src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdkZOwIZHlT9PnIQbhI4e6dwIjNqfz0vw"></script>
<script type='text/javascript' src="{{ asset('js/ext/jquery.ui.map.full.min.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/ext/jquery.maskedinput.js') }}"></script>
<script type='text/javascript' src="{{ asset('js/maskedinput.js') }}"></script>

<!-- address lookup -->
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script>-->
<script type='text/javascript' src="{{ asset('js/map-suggest-inline.js') }}"></script>
@stop
